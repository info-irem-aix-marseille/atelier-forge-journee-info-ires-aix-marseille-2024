# Site "basique" avec Mkdocs

Ce site va permettre d'entrevoir les possibilités d'éditions
de contenus de documentation. Le langage utilisé est Markdown
et la mise en page s'effectue automatiquement grâce à Mkdocs

## Markdown

>Markdown est un langage de balisage léger créer en 2004 par John Gruber
avec l'aide d'Aaron Swartz dans le but d'offrir une syntaxe facile à lire et
à écrire dans l'état dans sa forme non formatée.

*source : Wikipedia*

## Mkdocs

Mkdocs est un outil permettant de générer facilement et rapidement des sites
statiques à partir de fichiers écrits en markdown.

