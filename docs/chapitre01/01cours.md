---
author: Votre nom
title: Exemple de mise en page
---

## I. Paragraphe 1 :

!!! info "Mon info"

    Ma belle info qui doit être indentée
    

??? note "se déroule en cliquant dessus"

    Ma note indentée


??? tip "Astuce"

    Ma belle astuce indentée



### 1. Sous paragraphe 1

Texte 1.1

### 2. Sous paragraphe 2

Texte 1.2

## II. Paragraphe 2 :

texte 1

### 1. Sous paragraphe 1

Texte 2.1

### 2. Sous paragraphe 2

Texte 2.2
