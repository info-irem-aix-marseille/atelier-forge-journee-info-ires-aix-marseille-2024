# Présentation de  Simon ANDREYS

## Identité

- nom : Andreys     
- prénom : Simon

## Activité

J'enseigne les mathématiques au lycée Perier (Marseille)

## Motivation

Je suis intéressé(e) par la Forge parce que je veux profiter du travail des collègues partager le mien. Je veux faire aussi peu d'efforts que nécessaire, mais pas moins. 